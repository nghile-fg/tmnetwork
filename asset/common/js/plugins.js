
$(document).ready(function () {
    $(window).on("load resize scroll", function () {
        if ($(window).scrollTop() > $(window).height()) {
            $('.footer-backtop-btn, .footer-hotline').fadeIn();
        }
        if ($(window).scrollTop() < $(window).height()) {
            $('.footer-backtop-btn, .footer-hotline').fadeOut();
        }
    });
});